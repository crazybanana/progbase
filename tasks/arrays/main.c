#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include <time.h>
#include <math.h>

int main(void){
  int arr[10];
  char S[10];
  int i = 0;
  int count = 0;
  int sum = 0;
  int index = 0;
  int min = 0;
  int max = 0;
  float ave = 0.0;

  srand(time(0));
  printf("1.Array: \n");
  for (i = 0; i < 10; i++){
    arr[i] = rand() % 300 - 100;
    printf("%i ", arr[i]);
    }

  printf("\n2.Numbers garether than '-25': \n");
  for (i = 0; i < 10; i++){
    if(arr[i] > - 25){
      conSetAttr(BG_GREEN);
      printf("%i ", arr[i]);
      conReset();
    }else{
      printf("%i ", arr[i]);
    }
  }

  printf("\n3.Numbers less than '-50' and gather than '100': \n");
  for (i = 0; i < 10; i++){
    if((arr[i] < - 50) || (arr[i] > 100)){
      count++;
      sum += arr[i];
      conSetAttr(BG_INTENSITY_RED);
      printf("%i ", arr[i]);
      conReset();
    }else{
      printf("%i ", arr[i]);
    }
  }
  ave = sum / (float) count;
  printf("\nCount: %i\nSum: %i\nAve: %f", count, sum, ave);

  min = arr[0];
  printf("\n4.Min: \n");
  for(i = 0; i < 10; i++){
    if(arr[i] > -50 && arr[i] < 50){
      if(arr[i] < min){
        index = i;
        min = arr[i];
      }
    }
  }
  printf("Min: %i\nIndex: %i", min, index);

  min = arr[0];
  printf("\n5.Max: \n");
  index = 0;
  for(i = 0; i < 10; i++){
    if(arr[i] > max){
      index = i;
      max = arr[i];
    }
  }
  printf("Max: %i\nIndex: %i", max, index);

  printf("\n6.New array: \n");
  for(i = 0; i < 10; i++){
    S[i] = arr[i] % 95 + 32;

  }
  S[9] = '\0';
  puts(S);

  printf("7.arr to 0: \n");
  for(i = 0; i < 10; i++){
    if(arr[i] > 50){
      arr[i] = 0;
      printf("%i ", arr[i]);
    }else{
      printf("%i ", arr[i]);
    }
  }

  printf("\n");
  return 0;
}
