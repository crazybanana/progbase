#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

int seqMax(char * pStart, size_t seqLen);

int main(void) {
    char str[100];
    int biggest = 0;
    printf("PLease, enter a string: ");
    fgets(str, 100, stdin);
    str[strlen(str) - 1] = '\0';
    printf("\nYou've entered: %s\n", str);

    /* call recursive function and print it's result*/
    biggest = seqMax(str, strlen(str));
    printf("%s %c %3d\n\n","\nThe symbol whith biggest ASCII code number is: ", biggest, biggest);
    return EXIT_SUCCESS;
}

int seqMax(char * pStart, size_t seqLen) {
  int first = pStart[0];
  if (1 == seqLen) return first;
  else {
      int maxNext = seqMax(pStart + 1, seqLen - 1);
      return (first > maxNext) ? first : maxNext;
  }
}
