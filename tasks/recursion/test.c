#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>



int main ( void ){
    int biggest = 0, ch;
    printf("Enter the string: ");
    ch = getchar();
    while ((ch) != EOF) {
        if ( ch > biggest)
            biggest = ch;
        break;
    }

printf("%s %c %3d\n\n","\nThe symbol whith biggest ASCII code number is: ", biggest, biggest);
    return 0;
}
