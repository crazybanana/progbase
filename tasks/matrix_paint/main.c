#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>

char image[28][28] = {
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','2','2','2','2','6','6','6','6','6','6','6','6','6','2','2','2','2','6','6','6','6','6','6' },
    { '6','6','6','6','2','2','2','2','2','2','6','6','6','6','6','6','6','2','2','2','2','2','2','6','6','6','6','6' },
    { '6','6','6','2','2','2','2','2','2','2','2','6','6','6','6','6','2','2','2','2','2','2','2','2','6','6','6','6' },
    { '6','6','2','2','2','2','2','2','2','2','2','2','6','6','6','2','2','2','2','2','2','2','2','2','2','6','6','6' },
    { '6','2','2','2','2','2','2','D','2','2','2','2','2','2','2','2','D','2','2','2','D','2','2','2','2','2','6','6' },
    { '6','6','2','2','2','2','D','D','D','2','2','D','D','D','D','2','D','D','2','D','D','2','2','2','2','6','6','6' },
    { '6','6','6','2','2','D','2','D','2','D','2','D','2','2','D','2','D','2','D','2','D','2','2','2','6','6','6','6' },
    { '6','6','6','6','2','2','D','D','D','2','2','D','2','2','D','2','D','2','2','2','D','2','2','6','6','6','6','6' },
    { '6','6','6','6','6','2','2','D','2','2','2','D','2','2','D','2','D','2','2','2','D','2','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','2','2','2','2','2','2','2','2','2','2','2','2','2','2','2','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','6','2','2','2','2','2','2','2','2','2','2','2','2','6','6','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','6','6','2','2','2','2','2','2','2','2','2','2','6','6','6','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','6','6','6','2','2','2','2','2','2','2','2','6','6','6','6','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','6','6','6','6','2','2','2','2','2','2','6','6','6','6','6','6','6','6','6','6','6','6' },
    { '6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6','6' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
    { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
};

int main(void) {
    const char colorsTable[16][2] = {
      {'0', BG_INTENSITY_RED},
      {'1', BG_BLACK},
      {'2', BG_RED},
      {'3', BG_GREEN},
      {'6', BG_YELLOW},
      {'5', BG_BLUE},
      {'4', BG_MAGENTA},
      {'D', BG_CYAN},
      {'8', BG_WHITE},
      {'9', BG_INTENSITY_BLACK},
      {'A', BG_INTENSITY_GREEN},
      {'B', BG_INTENSITY_YELLOW},
      {'C', BG_INTENSITY_BLUE},
      {'7', BG_INTENSITY_MAGENTA},
      {'E', BG_INTENSITY_CYAN},
      {'F', BG_INTENSITY_WHITE}
    };
    int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    char colorsPalette[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
    int colorsImageLength = sizeof(image) / sizeof(image[0]);
    int x, y, j, i;
    int jdir, idir, count;
    int horizontalMax = 15;
    int horizontalMin = 11;
    int verticalMax = 15;
    int verticalMin = 11;
    int colorPairIndex = 0;
    conClear();

    for (i = 0; i < colorsPaletteLength; i++)
    {
        char colorCode = '\0';
        char color = '\0';
        colorCode = colorsPalette[i];
        for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
        {
            char colorPairCode = colorsTable[colorPairIndex][0];
            char colorPairColor = colorsTable[colorPairIndex][1];
            if (colorCode == colorPairCode)
            {
                color = colorPairColor;
                break;
            }
        }
        conSetAttr(color);
        putchar(' ');
    }
    puts("");
    conReset();

    for(i = 0; i < colorsImageLength; i++)
    {
      for(j = 0; j < colorsImageLength; j++)
      {
        char colorCode = '\0';
        char color = '\0';
        colorCode = image[i][j];
        x = 5 + i;
        y = 1 + j;
        for(colorPairIndex = 0; colorPairIndex < colorsImageLength; colorPairIndex++)
        {
          char colorPairCode = colorsTable[colorPairIndex][0];
          char colorPairColor = colorsTable[colorPairIndex][1];
          if(colorCode == colorPairCode)
          {
            color = colorPairColor;
            break;
          }
          conMove(x, y);
        }
        conSetAttr(color);
        putchar(' ');
      }
    }
    puts("");
    conReset();

    i = 13;
    j = 13;
    idir = 0;
    jdir = 1;
    for(count = 0; count < colorsImageLength * colorsImageLength; count++){
      char colorCode = '\0';
      char color = '\0';
      colorCode = image[i][j];
      x = 35 + j;
      y = 5 + i;
      fflush(stdout);
      sleepMillis(10);
      i += idir;
      j += jdir;

      if(j == horizontalMax){
        horizontalMax += 1;
        j--;
        i++;
        jdir = 0;
        idir = 1;
      }
      if(i == verticalMax){
        verticalMax += 1;
        i--;
        j--;
        idir = 0;
        jdir = -1;
      }
      if(j == horizontalMin){
        horizontalMin -= 1;
        j++;
        i--;
        jdir = 0;
        idir = -1;
      }
      if(i == verticalMin){
        verticalMin -= 1;
        i++;
        j++;
        idir = 0;
        jdir = 1;
      }
      for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
      {
          char colorPairCode = colorsTable[colorPairIndex][0];
          char colorPairColor = colorsTable[colorPairIndex][1];
          if (colorCode == colorPairCode)
          {
              color = colorPairColor;
              break;
          }
      }
      conMove(y, x);
      conSetAttr(color);
      putchar(' ');
    }
    puts("");
    conReset();

    for(i = 0; i < colorsImageLength; i++){
      for(j = 0; j < colorsImageLength; j++){
        char tmp = '\0';
        tmp = image[i][j];
        image[i][j] = image[j][i];
        image[j][i] = tmp;
      }
    }

    for(i = 1; i < colorsImageLength; i++){
      for(j = 0; j < colorsImageLength; j++){
        char colorCode = '\0';
        char color = '\0';
        x = 5 + j;
        y = 69 + i;
        colorCode = image[i][j];
        for(colorPairIndex = 0; colorPairIndex < colorsImageLength; colorPairIndex++)
        {
          char colorPairCode = colorsTable[colorPairIndex][0];
          char colorPairColor = colorsTable[colorPairIndex][1];
          if(colorCode == colorPairCode)
          {
            color = colorPairColor;
            break;
          }
          conMove(x, y);
        }
        conSetAttr(color);
        putchar(' ');
      }
    }
    puts("");
    conReset();
    return 0;
}
