#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
    STRING_SIZE = 50
};

int main(void) {
    char * inputStr     = NULL;
    char * vowelCharPtr = NULL;
    int  * count        = NULL;

    inputStr = (char *) malloc(STRING_SIZE * sizeof(char));
    count    = (int  *) malloc( 1 * sizeof(int ));
    if (NULL == inputStr || NULL == count){
        printf("Alloc error");
        return EXIT_FAILURE;
    }

    *count = 0;
    printf("Please, input string: ");
    fgets(inputStr, STRING_SIZE, stdin);

    vowelCharPtr = strpbrk(inputStr, inputStr);
    while (NULL != vowelCharPtr) {
        if(vowelCharPtr == inputStr){
        (*count) ++;
        vowelCharPtr = strpbrk(vowelCharPtr + 1, inputStr);
      }
    }

    printf("Вывести последний из символов, что повторяется больше 1 раза подряд.\n");
    printf("Number of vowels: %i\n", *count);

    free(inputStr);
    free(count);

    return EXIT_SUCCESS;
}
