#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>

int filter(char ch){
  int i = 0;
  int k = 0;
  char filter[50] = "BbCcDdFfGgHhKkLlMmNnPpRrSsTtVvWwXxZz";
  for(i = 0; i < strlen(filter); i++){
    if(ch == filter[i]){
      k = 1;
      break;
    }else{
      k = 0;
    }
  }
  if(k == 1){
    return 1;
  }else{
    return 0;
  }
}

int main(void){
  int i = 0;
  char text[] = " When I’ve watched through his eyes,\
I’ve listened through his ears, and I tell you he’s the one. \
Or at least as close as we’re going to get. That’s what you said about the brother.\
The brother tested out impossible. For other reasons. Nothing to do with his ability.\
Same with the sister. And there are doubts about him. He’s too malleable.\
Too willing to submerge himself in someone else’s will.\
Not if the other person is his enemy.\
So what do we do? Surround him with enemies all the time?";

  char buf[700] = "";
  int k = 0;
  int j = 0;
  int wordStart = 0;
  int count = 0;
  int wordLen = 0;

  printf("==================\n");
  puts(text);
  printf("Length of text: %i\n", (int)strlen(text));
  printf("==================\n");

  for(i = 0; i < strlen(text); i++){
    char ch = text[i];
    if(text[i] != 'v' && text[i] != 'w' && text[i] != 't' && text[i] != 'c' &&
    text[i] != 'h' && text[i] != 'r' && text[i] != 'd' && text[i] != 's' &&
    text[i] != 'l' && text[i] != 'm' && text[i] != 'b' && text[i] != 'g' &&
    text[i] != 'f' && text[i] != 'T' && text[i] != 'H' && text[i] != 'k' &&
    text[i] != 'p' && text[i] != 'n' && text[i] != 'W' && text[i] != 'N' &&
    text[i] != 'S' && text[i] != 'F'){
      buf[k] = ch;
      k++;
    }
  }
  puts(buf);
  printf("Length of text: %i\n", (int)strlen(buf));
  printf("==================\n");
  printf("------------------\n");

  for(i = 0; i < strlen(text); i++){
    char ch = text[i];
    if(ch == '.' || ch == '?' || ch == '!'){
      k = i - wordStart;
      if(k > 0){
        for(j = wordStart; j < i; j++){
          buf[j - wordStart] = text[j];
        }
        buf[k] = '\0';
        puts(buf);
        printf("Length of sentence: %i", (int)strlen(buf));
        printf("\n------------------\n");
      }
      wordStart += k + 1;
    }
  }
  printf("==================\n");

  for(i = 0; i < strlen(text); i++){
    char ch = text[i];
    if(ch == ' '){
      k++;
    }
  }
  printf("Amount of the words: %i\n", k);
  printf("==================\n");

  wordStart = 0;
  for(i = 0; i < strlen(text); i++){
    char ch = text[i];
    if(!isalpha(ch)){
      wordLen = i - wordStart;
      if(wordLen > 0 && wordLen <= 8){
        for(k = wordStart; k < i; k++){
          buf[k - wordStart] = text[k];
        }
        buf[wordLen] = '\0';
        for (j = 0; j < wordLen; j++){
          printf("%c", buf[j]);
          if (j == wordLen - 1){
            printf(", ");
          }
        }
        count++;
      }
      wordStart += wordLen + 1;
    }
  }
  printf("\nThe amount of the words less than 8: %i\n", count);
  printf("==================\n");

  for (i = 0; i < strlen(text); i++){
    char ch = text[i];
    if (!isalpha(ch)){
      wordLen = i - wordStart;
      if (wordLen > 0){
        for (k = wordStart; k < i; k++){
          buf[k - wordStart] = text[k];
        }
        buf[wordLen] = '\0';
        for (j = 0; j < wordLen; j++){
          if ((1 == filter(buf[0])) && (1 == filter(buf[wordLen-1]))){
            printf("%c", buf[j]);
            if (j == wordLen - 1)
            {
              printf(", ");
              count++;
            }
          }
        }
      }
      wordStart += wordLen + 1;
    }
  }
  printf("\nThe amount of the words: %i\n", count);
  printf("==================\n");

  return 0;
}
