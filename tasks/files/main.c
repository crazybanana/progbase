#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


enum {
    BUFFER_SIZE = 1000
};

int file_process(const char * readFileName, const char * writeFileName);

int main(void) {
    const char * filename = "readFileName.txt";
    char buffer[BUFFER_SIZE];
    long position = 0;
    FILE * fin = fopen(filename, "r");
    if (fin == NULL) {
   	 printf("Error opening file %s\n", filename);
   	 return EXIT_FAILURE;
    }
    fseek(fin, 0, SEEK_END);
    position = ftell(fin);
    fseek(fin, 0, SEEK_SET);
    printf("%s (%ld bytes)\n", filename, position);
    while (!feof(fin)) {
   	 fgets(buffer, BUFFER_SIZE, fin);
     if(buffer == '\n'){
   	 buffer[strlen(buffer) - 1] = '\0';
   	 printf("%s\n", buffer);
   }
    }
    fclose(fin);
    return EXIT_SUCCESS;
}

int file_process(const char * readFileName, const char * writeFileName){
  const char * readFileName = "";

}
