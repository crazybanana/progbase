#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define SIZE 4

struct Film{
  char name[100];
  int year;
  double budget;
  int awards;
}movie;

int search_year(struct Film movie[], int year){
  int count = 0;
  for(int i = 0; i < SIZE; i++){
    if(year > movie[i].year){
      count++;
    }
  }
  return count;
}

char * find_best_movie(struct Film movie[], char * buffer){
  int i;
  int max = movie[0].awards;
  int find_index;
  struct Film find_movie;
  for(i = 0; i < SIZE; i++)
  {
    if(movie[i].awards > max)
    {
      max = movie[i].awards;
      find_index = i;
    }
  }
  find_movie = movie[find_index];
  while(find_movie.name[i] != '\0')
  {
    buffer[i] = find_movie.name[i];
    i += 1;
  }
  return buffer;
}

struct Film * maxBudget(struct Film movie[], int size){
  int i;
  int find_index = 0;
  struct Film *find_movie;
  double max = movie[0].budget;
  for(i = 0; i < SIZE; i++){
    if(movie[i].budget > max)
    {
      max = movie[i].budget;
      find_index = i;
    }
  }
  find_movie = &movie[find_index];
  return find_movie;
}

int main(void){
  struct Film * p;
  struct Film find_budget;
  struct Film movie[SIZE] = {
    {"Аватар", 2012, 57332243, 7},
    {"Варкрафт", 2016, 23049849, 3},
    {"Матрица", 2008, 1000000, 0},
    {"Титаник", 2007, 83049849, 15}
  };
  char buffer[100];
  int result1 = search_year(movie, 2010);
  assert(result1 == 2);
  p = maxBudget(movie, 4);
  find_budget = (*p);
  assert(find_budget.budget == 83049849);
  assert(find_best_movie(movie, buffer) == "Титаник");
  return EXIT_SUCCESS;
}
