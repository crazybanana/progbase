#include <stdlib.h>
#include <stdio.h>
#define SIZE 3

int search_pos_sum(int arr[SIZE][SIZE]){
  int sum = 0;
  for(int i = 0; i < SIZE; i++){
    for(int j = 0; j < SIZE;){
      if(arr[i][j] >= 0){
        sum += arr[i][j];
      }
      j++;
    }
    printf("%i\n", sum);
    sum = 0;
  }
  return sum;
}

int main(void){
  int array[SIZE][SIZE] = {
    {1, 2, 3},
    {3, 4, 5},
    {6, 7, 8},
  };
  search_pos_sum(array);
  return EXIT_SUCCESS;
}
