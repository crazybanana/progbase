#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

int search_sum(char *str){
  int sum = 0;
  char ch;
  for(int i = 0; i < strlen(str); i++){
    ch = str[i];
    if(isalpha(ch) && islower(ch)){
      sum += ch;
    }
  }
  return sum;
}

int main(void){
  char string1[] = "AB";
  int sum1 = search_sum(string1);
  assert(sum1 == 0);
  char string2[] = "12";
  int sum2 = search_sum(string2);
  assert(sum2 == 99);
  char string3[] = "fjsd";
  int sum3 = search_sum(string3);
  assert(sum3 == 423);
  char string4[] = "bla";
  int sum4 = search_sum(string4);
  assert(sum4 == 303);
  char string5[] = "!";
  int sum5 = search_sum(string5);
  assert(sum5 == 33);
  return EXIT_SUCCESS;
}
