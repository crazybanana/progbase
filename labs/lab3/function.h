#ifndef FUNCTION_H_INCLUDED
#define FUNCTION_H_INCLUDED

  struct Writer{
   char name[50];
   int bornYear;
   float bornDateMonth;
   struct {
    char bookName[50];
    int publicationYear;
  } book;
 };

  int input();

  int toFile(int size, struct Writer * wr[]);

  int fromFile(int size, const char *file_in, struct Writer * wr[]);

  void search(int size, struct Writer * wr[]);

  void change(struct Writer * wr[]);

  void copy(struct Writer * wr[]);

  struct Writer *deleteStr(struct Writer * wr[], int input);

  void fromString(int size, struct Writer * wr[]);

  char strScan(char *str);

  char strGets(char *str);

  void showData(int size, struct Writer * wr[]);

  void getData(int size, struct Writer * wr[]);

#endif
