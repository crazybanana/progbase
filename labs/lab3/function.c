#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pbconsole.h>
#include <ctype.h>
#include <string.h>
#include "function.h"

  void getData(int size, struct Writer * wr[]){
    int i = 0;
    char character;
    for(i = 0; i < size; i++){
      wr[i] = (struct Writer*)malloc(sizeof(struct Writer));
      printf("Введите имя автора и инициалы(через точку): ");
      scanf("%s", wr[i]->name);
      while ((character = getchar()) != '\n' && character != EOF) { }
      printf("Введите его год рождения: ");
      scanf("%i", &wr[i]->bornYear);
      while ((character = getchar()) != '\n' && character != EOF) { }
      printf("Введите его дату и месяц рождения(через точку): ");
      scanf("%f", &wr[i]->bornDateMonth);
      while ((character = getchar()) != '\n' && character != EOF) { }
      printf("Введите название книги: ");
      scanf("%s", wr[i]->book.bookName);
      while ((character = getchar()) != '\n' && character != EOF) { }
      printf("Введите год ее публикации: ");
      scanf("%i", &wr[i]->book.publicationYear);
      while ((character = getchar()) != '\n' && character != EOF) { }
    }
  }

  char strGets(char *str) {
    fgets(str, 200, stdin);
    return *str;
  }

  char strScan(char *str) {
    scanf("%s",str);
    return *str;
  }

  int toFile(int size, struct Writer * wr[]){
    char file_out[100];
    printf("Введите имя файла куда хотите записать: ");
    scanf("%s", file_out);
    char *write = file_out;
    FILE *fout = fopen(write, "w");
    if (file_out == NULL ){
  		  puts("ERROR, NO FILE");
  		    return 1;
      }
    for(int i = 0; i < size; i++){
      if(wr[i] != NULL){
        fprintf(fout,"%s %i %f %s %i\n", wr[i]->name, wr[i]->bornYear, wr[i]->bornDateMonth, wr[i]->book.bookName, wr[i]->book.publicationYear);
      }
    }

  		fclose(fout);
  		return 0;
  }

  int fromFile(int size, const char *file_in, struct Writer * wr[]){
    enum{BUFFER_SIZE = 100};
		char buffer[BUFFER_SIZE];
    int i = 0;
		FILE *fin = fopen(file_in, "r");
		if (fin == NULL ){
			return 1;
		}
		while (fgets(buffer, BUFFER_SIZE, fin) != NULL) {
			buffer[strlen(buffer) - 1] = '\0';
      for(i = 0; i < size; i++){
        if(wr[i] == NULL){
          wr[i] = (struct Writer*)malloc(sizeof(struct Writer));
          break;
        }
      }
      sscanf(buffer,"%s %i %5f %s %i",wr[i]->name, &wr[i]->bornYear, &wr[i]->bornDateMonth, wr[i]->book.bookName, &wr[i]->book.publicationYear);
  	}
		fclose(fin);
		return 0;
  }

  void fromString(int size, struct Writer * wr[]) {
    char buffer[200];
    int i;
    for (i = 0; i < size; i++){
      if(wr[i] == NULL){
        strGets(buffer);
        wr[i] = (struct Writer*)malloc(sizeof(struct Writer));
        sscanf(buffer,"%s %i %3f %s %i",wr[i]->name, &wr[i]->bornYear, &wr[i]->bornDateMonth, wr[i]->book.bookName, &wr[i]->book.publicationYear);
      }
    }
  }

  void showData(int size, struct Writer * wr[]){
    int i = 0;
    for(i = 0; i < size; i++){
      printf("\nЕго имя: %s\n", wr[i]->name);
      printf("Год рождения: %i\n", wr[i]->bornYear);
      printf("День и месяц: %.2f\n", wr[i]->bornDateMonth);
      printf("Название его книги: %s\n", wr[i]->book.bookName);
      printf("Год ее пубуликации: %i\n\n", wr[i]->book.publicationYear);
    }
  }

  int input(){
    int input;
    char character;
    printf("Введите индекс структуры которую хотите удалить: ");
    scanf("%i", &input);
    while ((character = getchar()) != '\n' && character != EOF) { }
    conClear();
    return input;
  }

  struct Writer * deleteStr(struct Writer * wr[], int input){
    struct Writer * wrt = wr[input];
    wr[input] = NULL;
    return wrt;
    free(wr[input]);
  }

  void copy(struct Writer * wr[]){
    int input;
    int output;
    char character;
    printf("Выберите индекс структуры откуда копировать\n");
    printf("Ваш ввод: ");
    scanf("%i", &input);
    while ((character = getchar()) != '\n' && character != EOF) { }
    printf("Выберите индекс структуры куда копировать\n");
    printf("Ваш ввод: ");
    scanf("%i", &output);
    while ((character = getchar()) != '\n' && character != EOF) { }
    conClear();
    if(wr[input] == NULL){
      printf("Нету такой структуры\n");
    }
    if(wr[output] == NULL){
      wr[input] = (struct Writer*)malloc(sizeof(struct Writer));
    }
    strcpy(wr[input]->name, wr[output]->name);
    wr[input]->bornYear = wr[output]->bornYear;
    wr[input]->bornDateMonth = wr[output]->bornDateMonth;
    strcpy(wr[input]->book.bookName, wr[output]->book.bookName);
    wr[input]->book.publicationYear = wr[output]->book.publicationYear;
  }

  void change(struct Writer * wr[]){
    int struct_n = 0;
    char character;
    int option;
    char buffer[200];
    float bornDateMonth;
    int bornYear;
    int publicationYear;
    printf("Выберите индекс структуры которую хотите изменить\n");
    printf("Ваш ввод: ");
    scanf("%i", &struct_n);
    while ((character = getchar()) != '\n' && character != EOF) { }
    if(wr[struct_n] == NULL && isdigit(struct_n)){
      wr[struct_n] = (struct Writer*)malloc(sizeof(struct Writer));
    }
    puts("Введите какое поле хотите изменить от 1 до 5 или 6 для выхода: ");
    printf("Ваш ввод: ");
    scanf("%i", &option);
    while ((character = getchar()) != '\n' && character != EOF) { }
    switch (option) {
      case 1:
        puts("Введите имя автора");
        strScan(buffer);
        sscanf(buffer,"%s",wr[struct_n]->name);
        break;
      case 2:
        puts("Введите год его рождения");
        scanf("%i", &bornYear);
        if(isalpha(bornYear) !=0 && ispunct(bornYear) != 0){
          puts("Это не цифры");
        }
        else{
          wr[struct_n]->bornYear = bornYear;
        }
        break;
      case 3:
      puts("Введите его день и месяц рождения");
        scanf("%f", &bornDateMonth);
        if(isalpha(bornDateMonth) !=0 && ispunct(bornDateMonth) != 0){
          puts("Это не цифры");
        }
        else{
          wr[struct_n]->bornDateMonth = bornDateMonth;
        }
      break;
      case 4:
        puts("Введите название его книги");
        strScan(buffer);
        sscanf(buffer,"%s",wr[struct_n]->book.bookName);
        break;
      case 5:
        puts("Введите год публикации книги");
        scanf("%i", &publicationYear);
        if(isalpha(publicationYear) !=0 && ispunct(publicationYear) != 0){
          puts("Это не цифры");
        }
        else{
          wr[struct_n]->book.publicationYear = publicationYear;
        }
        break;
      default:
        puts("ERROR");
        break;
      case 6:
        break;
    }
    conClear();
  }

  void search(int size, struct Writer * wr[]){
    int i = 0;
    char character;
    int year = 0;
    char buffer[100];
    printf("Введите год: ");
    scanf("%i", &year);
    while ((character = getchar()) != '\n' && character != EOF) { }
    conClear();
    for(i = 0; i < size; i++){
      if(wr[i]->bornYear < year){
        strcat(buffer, wr[i]->name);
        strcat(buffer, " ");
      }
    }
    printf("\nЭти автора родились до %i: ", year);
    puts(buffer);
    puts("");
    for(i = 0; i < strlen(buffer); i++){
      buffer[i] = 0;
    }
  }
