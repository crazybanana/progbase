#include <assert.h>
#include"assert_func.h"
#include "function.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

    void test (struct Writer * user[]){
      assert( fromFile (" ", user) == 1);
      assert( fromFile ("read.txt",user ) == 0);
      assert( fromFile ("*read.exe*", user) == 1);
    }

    void testDel (struct Writer * user[]){
      assert( deleteStr(user, 1) == NULL);
      assert( deleteStr(user, 25) == NULL);
      assert( deleteStr(user, -5) != NULL);
    }
