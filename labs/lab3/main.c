#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include "function.h"
#include <stdbool.h>
#include <string.h>
#include "assert_func.h"

int main(void){
  int on = 0;
  int a = 0;
  int b = 0;
  int howMany;
  char character;
  struct Writer * user[1000] = {NULL};
  conClear();
  printf("Выберите опцию:\n");
  printf("1.Создать новый массив данных\n");
  printf("2.Считать массив данных из файла\n");
  printf("3.Выход\n");
  printf("Ваш ввод: ");
  scanf("%i", &a);
  while ((character = getchar()) != '\n' && character != EOF) { }
  conClear();
  switch (a) {
    case 1:
      printf("Сколько структур хотите создать?\n");
      printf("Ваше число: ");
      scanf("%i", &howMany);
      while ((character = getchar()) != '\n' && character != EOF) { }
      if(user == NULL){
        showData(howMany, user);
      }
      conClear();
      getData(howMany, user);
      conClear();
      while(on != 1){
        showData(howMany, user);
        printf("Что теперь будем делать?\n");
        printf("1.Удалить структуру из массива\n");
        printf("2.Перезаписать данные структуры из другой\n");
        printf("3.Перезаписать определенное поле\n");
        printf("4.Найти писателей которые родились до выбраного года\n");
        printf("5.Записать структры в файл\n");
        printf("6.Вернуться\n");
        printf("Ваше число: ");
        scanf("%i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }
        conClear();
        showData(howMany, user);
        switch (b) {
          case 1:
            deleteStr(user, input());
            howMany--;
            break;
          case 2:
            copy(user);
            break;
          case 3:
            change(user);
            break;
          case 4:
            search(howMany, user);
            break;
          case 5:
            toFile(howMany, user);
            break;
          case 6:
            return main();
            break;
          default:
            printf("ERROR");
            break;
        }
      }
      break;
    case 2:
      printf("Сколько структур хотите считать?\n");
      printf("Ваше число: ");
      scanf("%i", &howMany);
      while ((character = getchar()) != '\n' && character != EOF) { }
      conClear();
      fromFile(howMany, "read.txt", user);
      while(on != 1){
        showData(howMany, user);
        printf("Что теперь будем делать?\n");
        printf("1.Удалить структуру из массива\n");
        printf("2.Перезаписать данные структуры из другой\n");
        printf("3.Перезаписать определенное поле\n");
        printf("4.Найти писателей которые родились до выбраного года\n");
        printf("5.Записать структры в файл\n");
        printf("6.Вернуться\n");
        printf("Ваше число: ");
        scanf("%i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }
        conClear();
        showData(howMany, user);
        switch (b) {
          case 1:
            deleteStr(user, input());
            howMany--;
            break;
          case 2:
            copy(user);
            break;
          case 3:
            change(user);
            break;
          case 4:
            search(howMany, user);
            break;
          case 5:
            toFile(howMany, user);
            break;
          case 6:
            return main();
            break;
          default:
            printf("ERROR");
            break;
        }
      }
      break;
    case 3:
      break;
    default:
      return main();
      break;
  }
  return 0;
}
