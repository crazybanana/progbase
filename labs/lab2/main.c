#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <progbase.h>
#include <pbconsole.h>

int main(void){
  int a;
  int b;
  int m = 0;
  char character;
  char ch;
  char charBuf;
  int arrSize = 10;
  int arrSize1 = 8;
  int i = 0;
  int j = 0;
  int k = 0;
  int maximum = 0;
  int counter;
  int array1[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int array2[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int array3[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  float array5[10] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  int array4[8][8] = {
                      {0, 0, 0, 0, 0, 0, 0, 0},
                      {0, 0, 0, 0, 0, 0, 0, 0}
                      };
  char buffer[80] = "Любая видимая строка";
  char buffer1[80] = "";
  int min = array1[0];
  int max = array1[0];
  int min1 = array2[0];
  int max1 = array2[0];
  int min2 = array4[0][0];
  int max2 = array4[0][0];
  int sum;
  int sum1;
  double sum2;
  int buf;
  float buf1;
  int index;
  int index1;
  int index2;
  int index3;

  conClear();
  conMove(3, 1);
  printf("Выберите задание:\n");
  printf("1.Одномерный массив\n");
  printf("2.Два одномерных массива\n");
  printf("3.Двумерный массив\n");
  printf("4.Строка\n");
  printf("5.Выход\n");

  printf("Ваш выбор: ");
  scanf("%i", &a);
  while ((character = getchar()) != '\n' && character != EOF) { }
  switch(a){
    case 1:
      conClear();
      while(m != 1){
        conMove(3, 1);
        printf("(Задание №1)\n");
        printf("Выберите операцию:\n");
        printf("1.Заполнить массив случайными числами от -100 до 100\n");
        printf("2.Обнулить все элементы массива\n");
        printf("3.Найти максимальный элемент и его индекс\n");
        printf("4.Найти сумму элементов массива\n");
        printf("5.Вывести сумму положительных чисел\n");
        printf("6.Найти первый элемент, что повторяется наибольшее количество раз\n");
        printf("7.Поменять максимальный и минимальный элемент массива\n");
        printf("8.Уменьшить все элементы массива на введенное число\n");
        printf("9.Назад в меню\n");

        printf("Ваш выбор: ");
        scanf(" %i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }
        switch(b){
          case 1:
            conClear();
            conMove(3, 80);
            srand(time(0));
            printf("Массив: ");
            for(i = 0; i < arrSize; i++){
              array1[i] = rand() %201 - 100;
              printf("%i ", array1[i]);
            }
            puts("");
            break;
          case 2:
            conClear();
            conMove(3, 80);
            printf("Массив: ");
            for(i = 0; i < arrSize; i++){
              array1[i] = 0;
              printf("%i ", array1[i]);
            }
            puts("");
            break;
          case 3:
            conClear();
            conMove(3, 80);
            max = array1[0];
            index = 0;
            for(i = 0; i < arrSize; i++){
              if(max < array1[i]){
                max = array1[i];
                index = i;
              }
            }
            printf("Массив: ");
            for (i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Максимальное число и его индекс: %i, %i\n", max, index);
            break;
          case 4:
            conClear();
            conMove(3, 80);
            sum = 0;
            for(i = 0; i < arrSize; i++){
              sum += array1[i];
            }
            printf("Массив: ");
            for (i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Сумма элементов массива: %i\n", sum);
            break;
          case 5:
            conClear();
            conMove(3, 80);
            sum = 0;
            for(i = 0; i < arrSize; i++){
              if(array1[i] > 0){
                sum += array1[i];
              }
            }
            printf("Массив: ");
            for (i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Сумма положительных элементов массива: %i\n", sum);
            break;
          case 6:
            conClear();
            conMove(3, 80);
            counter = 0;
            index = 0;
            buf = 0;
            max1 = array2[0];
            index = 0;
            for(k = 0; k < arrSize; k++){
              buf = array1[k];
              for(i = 0; i < arrSize; i++){
                if(buf == array1[i]){
                  counter++;
                }
              }
              array2[k] = counter;
              counter = 0;
            }
            for(j = 1; j < arrSize; j++){
              if(array2[j] > max1){
                max1 = array2[j];
                index = j;
              }
            }
            printf("Массив: ");
            for (i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Элемент повторяющийся наибольшее количество раз: %i\n", array1[index]);
            break;
          case 7:
            conClear();
            conMove(3, 80);
            index = 0;
            index1 = 0;
            max = array1[0];
            min = array1[0];
            printf("Массив: ");
            for(i = 0; i < arrSize; i++){
              if(max < array1[i]){
                max = array1[i];
                index = i;
              }
              if(min > array1[i]){
                min = array1[i];
                index1 = i;
              }
              printf("%i ", array1[i]);
            }
            puts("");
            buf = array1[index];
            array1[index] = array1[index1];
            array1[index1] = buf;
            conMove(4, 80);
            printf("Резульат: ");
            for(i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            break;
          case 8:
            conClear();
            conMove(3, 1);
            printf("(Задание №1)\n");
            printf("Выберите операцию:\n");
            printf("1.Заполнить массив случайными числами от -100 до 100\n");
            printf("2.Обнулить все элементы массива\n");
            printf("3.Найти максимальный элемент и его индекс\n");
            printf("4.Найти сумму элементов массива\n");
            printf("5.Вывести сумму положительных чисел\n");
            printf("6.Найти первый элемент, что повторяется наибольшее количество раз\n");
            printf("7.Поменять максимальный и минимальный элемент массива\n");
            printf("8.Уменьшить все элементы массива на введенное число\n");
            printf("9.Назад в меню\n");
            buf = 0;
            for(i = 0; i < arrSize; i++){
              array5[i] = array1[i];
            }
            conMove(3, 75);
            printf("Массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%.1f ", array5[i]);
            }
            puts("");
            conMove(13, 1);
            printf("Выберите число: ");
            scanf("%f", &buf1);
            conClear();
            conMove(3, 75);
            printf("Массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%.1f ", array5[i]);
            }
            puts("");
            conMove(4, 75);
            printf("Результат: ");
            for(i = 0; i < arrSize; i++){
              array5[i] -= buf1;
              printf("%.1f ", array5[i]);
            }
            puts("");
            break;
          case 9:
            return main();
          default:
            conClear();
            conMove(15, 1);
            printf("Неверно выбраное число\n");
            break;
        }
      }
    case 2:
      conClear();
      while(m != 1){
        conMove(3, 1);
        printf("(Задание №2)\n");
        printf("Выберите операцию\n");
        printf("1.Заполнить массив случайными числами от -20 до 20\n");
        printf("2.Обнулить все элементы массивов\n");
        printf("3.Сумма двух массивов\n");
        printf("4.Массив в котором максимальная сумма элементов\n");
        printf("5.Смена максимального элемента первого массива с минимальным второго\n");
        printf("6.Назад в меню.\n");

        printf("Ваш выбор: ");
        scanf("%i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }

        switch (b) {
          case 1:
            conClear();
            conMove(3, 80);
            srand(time(0));
            printf("Первый массив: ");
            for(i = 0; i < arrSize; i++){
              array1[i] = rand() %41 - 20;
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Второй массив: ");
            for(i = 0; i < arrSize; i++){
              array2[i] = rand() %41 - 20;
              printf("%i ", array2[i]);
            }
            puts("");
            break;
          case 2:
            conClear();
            conMove(3, 80);
            printf("Первый массив: ");
            for(i = 0; i < arrSize; i++){
              array1[i] = 0;
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Второй массив: ");
            for(i = 0; i < arrSize; i++){
              array2[i] = 0;
              printf("%i ", array2[i]);
            }
            puts("");
            break;
          case 3:
            conClear();
            conMove(3, 80);
            printf("Первый массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Второй массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%i ", array2[i]);
            }
            puts("");
            conMove(5, 80);
            printf("Результат: ");
            for(i = 0; i < arrSize; i++){
              array3[i] = array1[i] + array2[i];
              printf("%i ", array3[i]);
            }
            puts("");
            break;
          case 4:
            sum = 0;
            conClear();
            conMove(3, 80);
            printf("Первый массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%i ", array1[i]);
            }
            puts("");
            conMove(4, 80);
            printf("Второй массив: ");
            for(i = 0; i < arrSize; i++){
              printf("%i ", array2[i]);
            }
            puts("");
            for(i = 0; i < arrSize; i++){
              sum += array1[i];
            }
            for(i = 0; i < arrSize; i++){
              sum1 += array2[i];
            }
            conMove(5, 80);
            if(sum < sum1){
              printf("Второй массив больше: ");
              for(i = 0; i < arrSize; i++){
                printf("%i ", array2[i]);
              }
            }else{
              printf("Первый массив больше: ");
              for(i = 0; i < arrSize; i++){
                printf("%i ", array1[i]);
              }
            }
            puts("");
            break;
          case 5:
          buf = 0;
          index = 0;
          index1 = 0;
          min1 = array2[0];
          max = array1[0];
          conClear();
          conMove(3, 80);
          printf("Первый массив: \n");
          conMove(4, 80);
          for(i = 0; i < arrSize; i++){
            if(max < array1[i]){
              max = array1[i];
              index = i;
            }
            printf("%i ", array1[i]);
          }
          puts("");
          conMove(5, 80);
          printf("Второй массив: \n");
          conMove(6, 80);
          for(i = 0; i < arrSize; i++){
            if(min1 > array2[i]){
              min1 = array2[i];
              index1 = i;
            }
            printf("%i ", array2[i]);
          }
          puts("");
          buf = array1[index];
          array1[index] = array2[index1];
          array2[index1] = buf;
          conMove(7, 80);
          printf("Результат: \n");
          conMove(8, 80);
          for(i = 0; i < arrSize; i++){
            printf("%i ", array1[i]);
          }
          puts("");
          conMove(9, 80);
          for(i = 0; i < arrSize; i++){
            printf("%i ", array2[i]);
          }
          puts("");
            break;
          case 6:
            return main();
            break;
          default:
            conClear();
            conMove(12, 1);
            printf("Неверно выбраное число");
            break;
        }
      }
      break;
    case 3:
      conClear();
      while (m != 1) {
        conMove(3, 1);
        printf("(Задание №3)\n");
        printf("Выберите операцию: \n");
        printf("1.Заполнить массив случайными числами\n");
        printf("2.Обнулить массив\n");
        printf("3.Найти минимальный элемент и его индекс\n");
        printf("4.Найти сумму главной диагонале массива\n");
        printf("5.Найти сумму ряда за заданным индексом\n");
        printf("6.Поменять максимальный и минимальный элемент массива\n");
        printf("7.Сменить элемент в массиве на указаный\n");
        printf("8.Назад в меню\n");

        printf("Ваш выбор: ");
        scanf("%i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }

        switch (b) {
          case 1:
            conClear();
            srand(time(0));
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                array4[i][j] = rand() %21 - 10;
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            break;
          case 2:
            conClear();
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                array4[i][j] = 0;
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            break;
          case 3:
            conClear();
            min2 = array4[0][0];
            index = 0;
            index1 = 0;
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                if(min2 > array4[i][j]){
                  min2 = array4[i][j];
                  index = i;
                  index1 = j;
                }
              }
            }
            conMove(13, 80);
            printf("Минимальный элемент массива: %i\n", min2);
            conMove(14, 80);
            printf("Его индекс: Ряд = %i, Столбик = %i\n", index, index1);
            break;
          case 4:
            conClear();
            sum = 0;
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                if(i == j){
                  sum += array4[i][j];
                }
              }
            }
            conMove(13, 80);
            printf("Сумма: %i\n", sum);
            break;
          case 5:
            conClear();
            sum = 0;
            conMove(3, 1);
            printf("(Задание №3)\n");
            printf("Выберите операцию: \n");
            printf("1.Заполнить массив случайными числами\n");
            printf("2.Обнулить массив\n");
            printf("3.Найти минимальный элемент и его индекс\n");
            printf("4.Найти сумму главной диагонале массива\n");
            printf("5.Найти сумму ряда за заданным индексом\n");
            printf("6.Поменять максимальный и минимальный элемент массива\n");
            printf("7.Сменить элемент в массиве на указаный\n");
            printf("8.Назад в меню\n");
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            conMove(12, 1);
            printf("Выберите ряд: ");
            scanf("%i", &buf);
            conClear();
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                if(i == buf){
                  sum += array4[i][j];
                }
              }
            }
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            conMove(12, 80);
            printf("Сумма: %i\n", sum);
            break;
          case 6:
            min2 = array4[0][0];
            max2 = array4[0][0];
            buf = 0;
            index = 0;
            index1 = 0;
            index2 = 0;
            index3 = 0;
            conClear();
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                if(min2 > array4[i][j]){
                  min2 = array4[i][j];
                  index = i;
                  index1 = j;
                }
                if(max2 < array4[i][j]){
                  max2 = array4[i][j];
                  index2 = i;
                  index3 = j;
                }
              }
            }
            buf = array4[index][index1];
            array4[index][index1] = array4[index2][index3];
            array4[index2][index3] = buf;
            conMove(13, 80);
            for(i = 0; j < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 14, 80);
            }
            break;
          case 7:
            i = 0;
            j = 0;
            conClear();
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            conMove(3, 1);
            printf("(Задание №3)\n");
            printf("Выберите операцию: \n");
            printf("1.Заполнить массив случайными числами\n");
            printf("2.Обнулить массив\n");
            printf("3.Найти минимальный элемент и его индекс\n");
            printf("4.Найти сумму главной диагонале массива\n");
            printf("5.Найти сумму ряда за заданным индексом\n");
            printf("6.Поменять максимальный и минимальный элемент массива\n");
            printf("7.Сменить элемент в массиве на указаный\n");
            printf("8.Назад в меню\n");
            conMove(12, 1);
            printf("Выберите ряд: ");
            scanf("%i", &i);
            conMove(13, 1);
            printf("Выберите столбец: ");
            scanf("%i", &j);
            conMove(14, 1);
            printf("Введите число: ");
            scanf("%i", &buf);
            array4[i][j] = buf;
            conClear();
            conMove(3, 80);
            printf("Ваш массив: \n");
            conMove(4, 80);
            for(i = 0; i < arrSize1; i++){
              for(j = 0; j < arrSize1; j++){
                printf("%3i ", array4[i][j]);
              }
              printf("\n");
              conMove(i + 5, 80);
            }
            conMove(13, 80);
            break;
          case 8:
            conClear();
            return main();
            break;
          default:
            conClear();
            conMove(14, 1);
            printf("Неверно выбраное число");
            break;
        }
      }
      break;
    case 4:
      conClear();
      while(m != 1){
        conMove(3, 1);
        printf("(Задание №4)\n");
        printf("Выберите операцию:\n");
        printf("1.Заполнить строку вводом из консоли\n");
        printf("2.Очистить строку\n");
        printf("3.Вывести длину строку\n");
        printf("4.Вывести подстроку из указаной позиции и указанной длинны\n");
        printf("5.Вывести список подстрок, разделёных указаным символом\n");
        printf("6.Вывести самое длинное слово\n");
        printf("7.Вывести все целые числа, содержащиеся в строке.\n");
        printf("8.Найти и вывести сумму всех дробных чисел, содержащихся в строке\n");
        printf("9.Назад в меню\n");

        printf("Ваш ввод: ");
        scanf("%i", &b);
        while ((character = getchar()) != '\n' && character != EOF) { }
        switch (b) {
          case 1:
            conClear();
            conMove(3, 1);
            for(i = 0; i < strlen(buffer); i++){
              buffer[i] = 0;
            }
            printf("(Задание №4)\n");
            printf("Выберите операцию:\n");
            printf("1.Заполнить строку вводом из консоли\n");
            printf("2.Очистить строку\n");
            printf("3.Вывести длину строку\n");
            printf("4.Вывести подстроку из указаной позиции и указанной длинны\n");
            printf("5.Вывести список подстрок, разделёных указаным символом\n");
            printf("6.Вывести самое длинное слово\n");
            printf("7.Вывести все целые числа, содержащиеся в строке.\n");
            printf("8.Найти и вывести сумму всех дробных чисел, содержащихся в строке\n");
            printf("9.Назад в меню\n");
            conMove(13, 1);
            printf("Введите строку(максимум 80 символов): ");
            fgets(buffer, 81, stdin);
            while ((character = getchar()) != '\n' && character != EOF) { }
            conClear();
            conMove(3, 80);
            puts("Ваша строка: ");
            conMove(4, 80);
            for(i = 0; i < strlen(buffer); i++){
              ch = buffer[i];
              printf("%c", ch);
            }
            conMove(14, 1);
            break;
          case 2:
            conClear();
            for(i = 0; i < strlen(buffer); i++){
              buffer[i] = 0;
            }
            conMove(3, 80);
            printf("Ваша строка очищена");
            break;
          case 3:
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(5, 80);
            printf("Длинна строки: %i символа\n", (int)strlen(buffer));
            break;
          case 4:
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(3, 1);
            printf("(Задание №4)\n");
            printf("Выберите операцию:\n");
            printf("1.Заполнить строку вводом из консоли\n");
            printf("2.Очистить строку\n");
            printf("3.Вывести длину строку\n");
            printf("4.Вывести подстроку из указаной позиции и указанной длинны\n");
            printf("5.Вывести список подстрок, разделёных указаным символом\n");
            printf("6.Вывести самое длинное слово\n");
            printf("7.Вывести все целые числа, содержащиеся в строке.\n");
            printf("8.Найти и вывести сумму всех дробных чисел, содержащихся в строке\n");
            printf("9.Назад в меню\n");
            printf("Выберите откуда начать: ");
            scanf("%i", &index);
            while ((character = getchar()) != '\n' && character != EOF) { }
            printf("Выберите где закончить: ");
            scanf("%i", &index1);
            while ((character = getchar()) != '\n' && character != EOF) { }
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(5, 80);
            printf("Результат: ");
            conMove(6, 80);
            for(i = index; i <= index1; i++){
              printf("%c", buffer[i]);
            }
            break;
          case 5:
            k = 0;
            index = 0;
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(3, 1);
            printf("(Задание №4)\n");
            printf("Выберите операцию:\n");
            printf("1.Заполнить строку вводом из консоли\n");
            printf("2.Очистить строку\n");
            printf("3.Вывести длину строку\n");
            printf("4.Вывести подстроку из указаной позиции и указанной длинны\n");
            printf("5.Вывести список подстрок, разделёных указаным символом\n");
            printf("6.Вывести самое длинное слово\n");
            printf("7.Вывести все целые числа, содержащиеся в строке.\n");
            printf("8.Найти и вывести сумму всех дробных чисел, содержащихся в строке\n");
            printf("9.Назад в меню\n");
            printf("Выберети знак: ");
            scanf("%c", &charBuf);
            while ((character = getchar()) != '\n' && character != EOF) { }
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(5, 80);
            printf("Результат: \n");
            conMove(6, 80);
            for(i = 0; i < strlen(buffer); i++){
              ch = buffer[i];
              if(ch == charBuf){
                puts("");
                conMove(k + 7, 80);
                k++;
              }else{
                printf("%c", ch);
              }
            }
            break;
          case 6:
            k = 0;
            index = 0;
            maximum = 0;
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            conMove(3, 1);
            printf("(Задание №4)\n");
            printf("Выберите операцию:\n");
            printf("1.Заполнить строку вводом из консоли\n");
            printf("2.Очистить строку\n");
            printf("3.Вывести длину строку\n");
            printf("4.Вывести подстроку из указаной позиции и указанной длинны\n");
            printf("5.Вывести список подстрок, разделёных указаным символом\n");
            printf("6.Вывести самое длинное слово\n");
            printf("7.Вывести все целые числа, содержащиеся в строке.\n");
            printf("8.Найти и вывести сумму всех дробных чисел, содержащихся в строке\n");
            printf("9.Назад в меню\n");
            for(i = 0; i < strlen(buffer); i++){
              ch = buffer[i];
              if(isalpha(ch)){
                k++;
              }else if(!isalpha(ch) || ch == ' '){
                if(maximum < k){
                  maximum = k;
                  index = i - k;
                }
                k = 0;
              }
            }
            conMove(5, 80);
            printf("Результат: ");
            conMove(6, 80);
            for(i = index; i < index + maximum; i++){
              printf("%c", buffer[i]);
            }
            break;
          case 7:
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            memset(buffer1, 0, 80);
            j = 0;
            conMove(5, 80);
            printf("Числа: ");
            for(i = 0; i < 80; i++){
              ch = buffer[i];
              if(isdigit(ch)){
                buffer1[j] = buffer[i];
                j++;
              } else if((ch == '.' || ch == ',') && isdigit(buffer[i + 1])){
                memset(buffer1, 0, 80);
                for(index = i;  index < 80 - i; index++){
                  charBuf = buffer[index];
                  if(!isdigit(buffer[index])){
                    i = index;
                    break;
                  }
                }
              } else if(j != 0){
                j = 0;
                conMove(6 + k, 80);
                puts(buffer1);
                memset(buffer1, 0, 80);
                k++;
              }
            }
            break;
          case 8:
            sum2 = 0;
            conClear();
            conMove(3, 80);
            printf("Ваша строка: \n");
            conMove(4, 80);
            printf("%s\n", buffer);
            memset(buffer1,0,80);
            j = 0;
            for(i = 0; i < 80; i++){
              ch = buffer[i];
              if(isdigit(ch)){
                buffer1[j] = buffer[i];
                j++;
              } else if((buffer[i] == '.' || buffer[i] == ',') && isdigit(buffer[i + 1])){
                buffer1[j] = buffer[i];
                j++;
              } else{
                for(index = 0; index < j; index++){
                  charBuf = buffer1[index];
                  if((charBuf == '.' || charBuf == ',') && isdigit(buffer1[index + 1])){
                    sum2 += atof(buffer1);
                    break;
                  }
                }
                memset(buffer1, 0, 80);
                j = 0;
              }
            }
            conMove(5, 80);
            printf("Сумма: %2f\n", sum2);
            break;
          case 9:
            conClear();
            return main();
            break;
          default:
            conClear();
            conMove(15, 1);
            printf("Неверно выбраное число");
            break;
        }
      }
      break;
    case 5:
      conClear();
      return 0;
      break;
    default:
      conClear();
      return main();
      break;
  }
  return 0;
}
