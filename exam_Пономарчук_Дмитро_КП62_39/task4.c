#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

void mystrcat(char* to, char* from);
size_t count(const char* str);
void removeChar(char * str);

int main(void){
  char str1[40];
  char str2[40];
  printf("Введите первую строку: ");
  fgets(str1, 40, stdin);
  printf("Введите вторую строку: ");
  fgets(str2, 40, stdin);
  removeChar(str1);
  puts(str1);
  puts(str1);
  return 0;
}

void mystrcat(char* to, char* from){
  while(*to){
    to++;
  }
  while(*from){
    *to = *from;
    to++;
    from++;
  }
}

size_t count(const char* str){
  return (*str) ? count(str++) + 1 : 0;
}

void removeChar(char * str){
  char *src, *dst, garbage, character;
  printf("Введите символ который нужно удалить: ");
  scanf("%c", &garbage);
  while ((character = getchar()) != '\n' && character != EOF) { }
    for(src = dst = str; *src != '\0'; src++){
    *dst = *src;
    if(*dst != garbage) dst++;
  }
  *dst = '\0';
}
