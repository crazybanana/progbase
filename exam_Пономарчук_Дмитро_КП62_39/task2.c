#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#define VAR 5

struct point2d {
  int x;
  int y;
};

struct point2d distSearch(struct point2d point[]) {
  int i;
  double distance[VAR];
  for ( i = 0; i < VAR; i++) {
    distance[i] = sqrt(pow(point[i].x, 2) + pow(point[i].y, 2) );
  }
  int min = 0;
  for (i = 0; i < VAR; i++) {
    if (distance[min] > distance[i] ) {
      min = i;
    }
  }
  return point[min];
}

int main()
{
  struct point2d point[VAR] = {
    {5, 4},
    {0, 1},
    {2, -1},
    {3, 10},
    {190, 750}
  };

  struct point2d min = {0, 1};

  struct point2d result = distSearch(point);

  assert(result.x == min.x);
  assert(result.y == min.y);

  struct point2d point1[VAR] = {
    {45, 4},
    {25, 0},
    {13, -1},
    {0, 0},
    {129, 734}
  };

  struct point2d min1 = {0, 0};
  result = distSearch(point1);
  assert(result.x == min1.x);
  assert(result.y == min1.y);

  return 0;
}
