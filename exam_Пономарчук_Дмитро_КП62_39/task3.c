#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <pbconsole.h>
#include <stdbool.h>
#include <string.h>

struct String{
  char str[40];
};

void fill(struct String * strings[]){
  for(int i = 0; i < 3; i++){
    strings[i] = (struct String*)malloc(sizeof(struct String));
    printf("Заполните массив строк: \n");
    scanf("%s", strings[i]->str);
  }
}

int searchStr(struct String * strings[], char* str){
  int maxI= -1;
  char buf[50];
  int max = 0;
  for(int i = 0; i < 5; i++){
    strcpy(buf, strings[i]->str);
    int k = 0;
    const char *tmp = buf;
    while(tmp = strstr(tmp, str)){
      k++;
      tmp++;
    }
    if(k > max){
      max = k;
      maxI = i;
    }
  }
  return maxI;
}

int main(void){

  return 0;
}
